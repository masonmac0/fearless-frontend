import React, { useEffect, useState } from 'react';

function PresentationForm( ) {
    const [conference, setConference] = useState([]);
    const fetchData = async () => {
    const url = 'http://localhost:8000/api/conferences/';

    const response = await fetch(url);
     // if response is okay lets get data using .json
     //method dont forget await
 //post
    if (response.ok){
        const data = await response.json();
        setConference(data.conferences)
    }
    };

    const[presenterName, setPresenterName] = useState('');
    const[companyName, setCompanyName] = useState('')
    const[presenterEmail, setPresenterEmail] = useState('')
    const[title, setTitle] = useState('')
    const[synopsis, setSynopsis] = useState('')
    const[conferences, setConferences] = useState('')


        const handlePresenterNameChange = (event) => {
            const value = event.target.value;
            setPresenterName(value);
            }
        const handleCompanyNameChange = (event) => {
            const value = event.target.value;
            setCompanyName(value);
            }
        const handlePresenterEmailChange = (event) => {
            const value = event.target.value;
            setPresenterEmail(value);
            }
        const handleTitleChange = (event) =>{
            const value = event.target.value;
            setTitle(value)
        }
        const handleSynopsisChange = (event) =>{
            const value = event.target.value;
            setSynopsis(value)
        }
        const handleConferenceChange = (event) =>{
            const value = event.target.value;
            setConferences(value)
        }

        const handleSubmit = async (event) => {
            event.preventDefault();

            // create an empty JSON object
            const data = {};

            data.presenter_name = presenterName;
            data.company_name = companyName;
            data.presenter_email = presenterEmail;
            data.title = title;
            data.synopsis = synopsis;

            console.log(data);

            const presentationUrl = `http://localhost:8000${conferences}presentations/`;
            const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
            };


        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation);

            setPresenterName('');
            setCompanyName('');
            setPresenterEmail('');
            setTitle('');
            setSynopsis('');
            setConferences('');
        }
    }


    useEffect(() => {
        fetchData();
        }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
            <div className="form-floating mb-3">
                <input onChange={handlePresenterNameChange} value={presenterName} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"></input>
                <label htmlFor="presenter_name">Presenter name</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handlePresenterEmailChange} value={presenterEmail} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"></input>
                <label htmlFor="presenter_email">Presenter email</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleCompanyNameChange} value={companyName} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"></input>
                <label htmlFor="company_name">Company name</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleTitleChange} value={title} placeholder="Title" required type="text" name="title" id="title" className="form-control"></input>
                <label htmlFor="title">Title</label>
            </div>
            <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea onChange={handleSynopsisChange} value={synopsis} className="form-control" id="synopsis" rows="3" name="synopsis" className="form-control"></textarea>
                </div>
                <div className="mb-3">
                <select onChange={handleConferenceChange} value={conferences} required name="conference" id="conference" className="form-select">
                    <option value="">Choose a conference</option>
                    {conference.map(conference =>{
                        return (
                            <option value={conference.href} key={conference.href} >
                            {conference.name}
                            </option>
                        );
                    })}
                </select>
            </div>
            <button className="btn btn-primary">Create</button>
            </form>
        </div>
        </div>
    </div>
    );
}

export default PresentationForm;
