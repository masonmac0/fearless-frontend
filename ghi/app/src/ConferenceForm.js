import React, { useEffect, useState } from 'react';

function ConferenceForm( ) {
    const [locations, setLocations] = useState([]);
    const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);
     // if response is okay lets get data using .json
     //method dont forget await
 //post
    if (response.ok){
        const data = await response.json();
        setLocations(data.locations)
    }
    };

    const[name, setName] = useState('');
    const[starts, setStartDate] = useState('')
    const[ends, setEndDate] = useState('')
    const[description, setDescription] = useState('')
    const[presentation, setPresentations] = useState('')
    const[attendees, setAttendees] = useState('')
    const[location, setLocation] = useState('')


        const handleNameChange = (event) => {
            const value = event.target.value;
            setName(value);
            }
        const handleStartDateChange = (event) => {
            const value = event.target.value;
            setStartDate(value);
            }
        const handleEndDateChange = (event) => {
            const value = event.target.value;
            setEndDate(value);
            }
        const handleDescriptionChange = (event) =>{
            const value = event.target.value;
            setDescription(value)
        }
        const handleMaxPresChange = (event) =>{
            const value = event.target.value;
            setPresentations(value)
        }
        const handleAttendee = (event) =>{
            const value = event.target.value;
            setAttendees(value)
        }
        const handleLocationChange = (event) =>{
            const value = event.target.value;
            setLocation(value)
        }

        const handleSubmit = async (event) => {
            event.preventDefault();

            // create an empty JSON object
            const data = {};

            data.name = name;
            data.starts = starts;
            data.ends = ends;
            data.description = description;
            data.max_presentations = presentation;
            data.max_attendees = attendees;
            data.location = location;

            console.log(data);

            const ConferenceUrl = 'http://localhost:8000/api/conferences/';
            const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
            };


        const response = await fetch(ConferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            setName('');
            setStartDate('');
            setEndDate('');
            setDescription('');
            setPresentations('');
            setAttendees('');
            setLocation('');
        }
    }


    useEffect(() => {
        fetchData();
        }, []);

    return (
    <div className="container">
    <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Create a new Conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"></input>
                <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleStartDateChange} value={starts} placeholder="Start Date" required type="date" name="starts" id="starts" className="form-control"></input>
                <label htmlFor="starts">Start Date</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleEndDateChange} value={ends} placeholder="End Date" required type="date" name="ends" id="ends" className="form-control"></input>
                <label htmlFor="ends">End Date</label>
            </div>
            <div className="mb-3">
                <label htmlFor="description">Description</label>
                <textarea onChange={handleDescriptionChange}value={description} placeholder="Description" required type="text" name="description" id="description" className="form-control" rows="5"></textarea >
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleMaxPresChange} value={presentation} placeholder="End Date" required type="number" name="max_presentations" id="max_presentations" className="form-control"></input>
                <label htmlFor="ends">Maximum presentations</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleAttendee} placeholder="Maximum Attendees" value={attendees} required type="number" name="max_attendees" id="max_attendees" className="form-control"></input>
                <label htmlFor="max_attendees">Maximum attendees</label>
            </div>
            <div className="mb-3">
                <select onChange={handleLocationChange} value={location} required id="location"  name="location" className="form-select">
                    <option value="" >Choose a Location</option>
                    {locations.map(location =>{
                        return (
                            <option value={location.id} key={location.id} >
                            {location.name}
                            </option>
                        );
                    })}
                </select>
            </div>
            <button className="btn btn-primary">Create</button>
            </form>
        </div>
        </div>
    </div>
    </div>
    );
}

export default ConferenceForm;
